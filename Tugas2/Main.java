package Kelas.tugas;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList <Mahasiswa> mahasiswaList = new ArrayList<>();

        while (true) {
            System.out.print("Masukkan nim (ketik 'selesai' untuk berhenti): ");
            String nim = scanner.nextLine();

            if (nim.equalsIgnoreCase("selesai")) {
                break;
            }

            System.out.print("Masukkan nama: ");
            String nama = scanner.nextLine();

            System.out.print("Masukkan alamat: ");
            String alamat = scanner.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa(nim, nama, alamat);
            mahasiswaList.add(mahasiswa);
        }

        System.out.println("==================================");
        System.out.println("No. | NIM             | Nama               | Alamat");
        System.out.println("----------------------------------");
        for (int i = 0; i < mahasiswaList.size(); i++) {
            Mahasiswa mahasiswa = mahasiswaList.get(i);
            System.out.printf("%-3d | %-10s | %-20s | %s\n", i + 1, mahasiswa.getNim(), mahasiswa.getNama(), mahasiswa.getAlamat());
        }
    }
}