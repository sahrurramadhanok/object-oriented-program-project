package Kelas.tugas3;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        // Membuat ArrayList untuk menyimpan mahasiswa
        ArrayList <mahasiswa> daftarMahasiswa = new ArrayList<>();

        // Menambahkan beberapa mahasiswa ke ArrayList
        daftarMahasiswa.add(new mahasiswa("Sahrur Ramadhan","235150400111010"));
        daftarMahasiswa.add(new mahasiswa("Supriyatno",     "215601239123914"));

        // Menambahkan beberapa mata kuliah untuk Sahrur Ramadhan
        mahasiswa sahrur = daftarMahasiswa.get(0);
        sahrur.addMataKuliah(new mataKuliah("Pemrograman Dasar", "COM60014", 95));
        sahrur.addMataKuliah(new mataKuliah("Arsitektur Komputer dan Sistem Operasi", "CIS61001", 78));

        // Menambahkan beberapa mata kuliah untuk Supriyatno
        mahasiswa supri = daftarMahasiswa.get(1);
        supri.addMataKuliah(new mataKuliah("Matematika Komputasi", "COM60015", 59));
        supri.addMataKuliah(new mataKuliah("Pengantar Keilmua Komputer", "IF202", 85));

        // Menampilkan informasi semua mahasiswa dan mata kuliahnya
        for (mahasiswa mhs : daftarMahasiswa) {
            System.out.println("===Kartu Hasil Studi===");
            System.out.println("Nama \t: " + mhs.getNamaMHS());
            System.out.println("NIM \t: " + mhs.getNim());
            for (mataKuliah mk : mhs.getDaftarMataKuliah()) {
                System.out.println(" - " + mk.getNamaMK() + " (" + mk.getKodeMK() + ") - Nilai: " + mk.getGrade());
            }
            System.out.println();
        }
    }
}
