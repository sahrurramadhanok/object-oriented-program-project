package Kelas.tugas3;

import java.util.ArrayList;

public class mahasiswa {
    String namaMHS;
    String nim;
    ArrayList <mataKuliah> daftarMataKuliah;

    //Setter class mahasiswa
    public mahasiswa (String namaMHS, String nim){
        this.namaMHS = namaMHS;
        this.nim = nim;
        daftarMataKuliah = new ArrayList<>();
    }

    //Getter untuk atribut classmahasiswa
    public String getNamaMHS() {
        return namaMHS;
    }


    public String getNim() {
        return nim;
    }


    public ArrayList<mataKuliah> getDaftarMataKuliah() {
        return daftarMataKuliah;
    }
    
    //method untuk menambahkan mata kuliah ke mahasiswa
    public void addMataKuliah(mataKuliah MK){
        daftarMataKuliah.add(MK);
    }
}
