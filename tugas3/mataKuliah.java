package Kelas.tugas3;

public class mataKuliah {
    String namaMK;
    String kodeMK;
    int nilaiAngka;
    char nilai;
    
    //Setter class mata kuliah
    public mataKuliah (String namaMK, String kodeMK, int nilaiAngka){
        this.namaMK = namaMK;
        this.kodeMK = kodeMK;
        this.nilaiAngka = nilaiAngka;
    }

    //getter untuk atribut class mata kuliah
    public String getNamaMK() {
        return namaMK;
    }
    public String getKodeMK() {
        return kodeMK;
    }
    public int getNilaiAngka() {
        return nilaiAngka;
    }
    
    //method untuk mengubak nilai angka menjadi nilai dengan format huruf
    public char getGrade(){
        int nilai = getNilaiAngka();
        
        if (nilai >= 80 && nilai <= 100) {
            return 'A';
        } else if (nilai >= 60 && nilai < 80) {
            return 'B';
        } else if (nilai >= 50 && nilai < 60) {
            return 'C';
        } else if (nilai >= 40 && nilai < 50) {
            return 'D';
        } else {
            return 'E';
        }
    }

    
    
}
